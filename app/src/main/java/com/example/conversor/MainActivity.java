package com.example.conversor;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.conversor.ui.main.SectionsPagerAdapter;

import static android.text.InputType.TYPE_CLASS_NUMBER;

public class MainActivity extends AppCompatActivity {

    //Variable global inicializada para indicar que la activity se inicia en el primer par de sistemas numericos.
    int state=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button menuBoton1 = findViewById(R.id.button1);
        Button menuBoton2 = findViewById(R.id.button2);
        Button menuBoton3 = findViewById(R.id.button3);
        TextView TextView1 = findViewById(R.id.textView1);
        TextView TextView2 = findViewById(R.id.textView2);
        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        ImageButton image1 = findViewById(R.id.imageButton1);
        ImageButton image2 = findViewById(R.id.imageButton2);
        editText2.setKeyListener(DigitsKeyListener.getInstance("01"));
        editText1.setKeyListener(DigitsKeyListener.getInstance("01234567"));

        //Esta linea es sólo un remanente de un intento de hacer el cambio de etiquetas animado:
        //Los archivos PageViewModel y PlaceHolderFrament también lo son.
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
    }

    //Este es el onClick del boton de la izda, el que realiza la conversión de arriba a abajo.
    public void boton1(View view){
        //Cargamos la vista de los dos editText para poder trabajar con ellos.
        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        //Creamos una variable decimal
        //(Aunque no convirtamos nunca a decimal va a ser el sistema que
        // utilicemos como intermedio para realizar la conversión).
        Long l;
        //Extraemos el texto del editText y lo convertimos en string para poder parsearlo.
        String s=editText1.getText().toString();
        //Si no hay texto no realizamos la conversión, y avisamos al usuario.
        if (s.equals("")){
            editText1.setError("Asegúrate de introucir un número");
        }
        else {
            switch (state) {
                //Conversión de Octal a Binario
                case 1:
                    try {
                        //Correción de errores
                        if(s.length()>14){
                            editText1.setError("El número es demasiado largo");
                        }
                        //Se pasa de base 8 a base 10 y después a base 2.
                        else {
                            l = Long.parseLong(s, 8);
                            editText2.setText(Long.toBinaryString(l));
                        }
                    }
                    //Más correción de errores.
                    catch (Exception e){
                        editText1.setError("El número es demasiado largo");
                    }
                    //^^^Esta estructura se repite en todos los cases y en los dos switches de los dos botones.^^^

                    /*
                    Esta es una versión primitiva de la conversión de octal a binario:

                    String conversionTable [] = { "000", "001", "010", "011", "100", "101", "110", "111"};
                    String myOctal=editText1.getText().toString();
                    editText2.setText("");
                    for (int i = 0; i <myOctal.length (); i ++) {
                        editText2.append(conversionTable [myOctal.charAt (i) - '0']);
                    }
                     */
                    break;
                case 2:
                    try {
                        if(s.length()>14){
                            editText1.setError("El número es demasiado largo");
                        }
                        else {
                            l = Long.parseLong(s, 8);
                            editText2.setText(Long.toHexString(l));
                        }
                    }
                    catch (Exception e){
                        editText1.setError("El número es demasiado largo");
                    }
                    break;
                case 3:
                    try {
                        l = Long.parseLong(s, 2);
                        editText2.setText(Long.toHexString(l));
                    }
                    catch (Exception e){
                        editText1.setError("El número es demasiado largo");
                    }
                    break;
            }
        }
    }

    //onClick del segund botón de conversión, que convierte de abajo a arriba.
    public void boton2(View view){
        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        Long l;
        String s=editText2.getText().toString();
        if (s.equals("")){
            editText2.setError("Asegúrate de introucir un número");
        }
        else {
            switch (state) {
                case 1:
                    try {
                        l = Long.parseLong(s, 2);
                        editText1.setText(Long.toOctalString(l));
                    }
                    catch (Exception e){
                        editText2.setError("El número es demasiado largo");
                    }
                    break;
                case 2:

                    try {
                        l = Long.parseLong(s, 16);
                        editText1.setText(Long.toOctalString(l));
                    }
                    catch (Exception e){
                        editText2.setError("El número es demasiado largo");
                    }
                    break;
                case 3:
                    try {
                        l = Long.parseLong(s, 16);
                        editText1.setText(Long.toBinaryString(l));
                    }
                    catch (Exception e){
                        editText2.setError("El número es demasiado largo");
                    }
                    break;
            }
        }
    }

    //onClick de la primera etiqueta del tab menu:
    public void menu1(View view) {
        Button menuBoton1 = findViewById(R.id.button1);
        Button menuBoton2 = findViewById(R.id.button2);
        Button menuBoton3 = findViewById(R.id.button3);
        TextView TextView1 = findViewById(R.id.textView1);
        TextView TextView2 = findViewById(R.id.textView2);
        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        ImageButton image1 = findViewById(R.id.imageButton1);
        ImageButton image2 = findViewById(R.id.imageButton2);
        //Cambio de color de las etiquetas para indicar cual está seleccionada.
        menuBoton1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        menuBoton2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        menuBoton3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        //Reseteo de campos, y configuración de los tipos de datos y filtros en el input.
        TextView1.setText("Octal:");
        TextView2.setText("Binario:");
        editText1.setText("");
        editText2.setText("");
        editText1.setHint("Escribe y toca 'OCT-BIN' :");
        editText2.setHint("Escribe y toca 'BIN-OCT' :");
        editText2.setInputType(TYPE_CLASS_NUMBER);
        editText1.setKeyListener(DigitsKeyListener.getInstance("01234567"));
        //Estructura de código para configurar los filtros de introducción de teclado.
        InputFilter inputFilter_moodMsg01 = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source.length()>44) return "";// max 44chars
                // Here you can add more controls, e.g. allow only hex chars etc (yes, I got this code from somewhere else)
                for (int i = start; i < end; i++) {
                    if (source.charAt(i)!='1' && source.charAt(i)!='0') {
                        return "";
                    }
                }
                return null;
            }
        };
        editText2.setFilters(new InputFilter[] { inputFilter_moodMsg01 });
        //Cambio de las imagenes de los ImageButton
        image1.setImageResource(R.drawable.octtobin);
        image2.setImageResource(R.drawable.bintooct);

        state=1;
    }

    public void menu2(View view) {
        Button menuBoton1 = findViewById(R.id.button1);
        Button menuBoton2 = findViewById(R.id.button2);
        Button menuBoton3 = findViewById(R.id.button3);
        TextView TextView1 = findViewById(R.id.textView1);
        TextView TextView2 = findViewById(R.id.textView2);
        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        ImageButton image1 = findViewById(R.id.imageButton1);
        ImageButton image2 = findViewById(R.id.imageButton2);
        menuBoton2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        menuBoton1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        menuBoton3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        TextView1.setText("Octal:");
        TextView2.setText("Hexadecimal:");
        editText1.setText("");
        editText2.setText("");
        editText1.setHint("Escribe y toca 'OCT-HEX' :");
        editText2.setHint("Escribe y toca 'HEX-OCT' :");
        editText1.setKeyListener(DigitsKeyListener.getInstance("01234567"));
        editText2.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        image1.setImageResource(R.drawable.octtohex);
        image2.setImageResource(R.drawable.hextooct);

        InputFilter inputFilter_moodMsg = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source.length()>44) return "";// max 44chars
                // Here you can add more controls, e.g. allow only hex chars etc
                for (int i = start; i < end; i++) {
                    if (source.charAt(i)!='1'
                        && source.charAt(i)!='0'
                        && source.charAt(i)!='2'
                        && source.charAt(i)!='3'
                        && source.charAt(i)!='4'
                        && source.charAt(i)!='5'
                        && source.charAt(i)!='6'
                        && source.charAt(i)!='7'
                        && source.charAt(i)!='8'
                        && source.charAt(i)!='9'
                        && source.charAt(i)!='A'
                        && source.charAt(i)!='B'
                        && source.charAt(i)!='C'
                        && source.charAt(i)!='D'
                        && source.charAt(i)!='E'
                        && source.charAt(i)!='F'
                        && source.charAt(i)!='a'
                        && source.charAt(i)!='b'
                        && source.charAt(i)!='c'
                        && source.charAt(i)!='d'
                        && source.charAt(i)!='e'
                        && source.charAt(i)!='f')
                    {
                        return "";
                    }
                }
                return null;
            }
        };
        editText2.setFilters(new InputFilter[] { inputFilter_moodMsg });

        state=2;
    }

    public void menu3(View view) {
        Button menuBoton1 = findViewById(R.id.button1);
        Button menuBoton2 = findViewById(R.id.button2);
        Button menuBoton3 = findViewById(R.id.button3);
        TextView TextView1 = findViewById(R.id.textView1);
        TextView TextView2 = findViewById(R.id.textView2);
        final EditText editText1 = findViewById(R.id.editText1);
        final EditText editText2 = findViewById(R.id.editText2);
        ImageButton image1 = findViewById(R.id.imageButton1);
        ImageButton image2 = findViewById(R.id.imageButton2);
        menuBoton3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        menuBoton1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        menuBoton2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        TextView1.setText("Binario:");
        TextView2.setText("Hexadecimal:");
        editText1.setText("");
        editText2.setText("");
        editText1.setHint("Escribe y toca 'BIN-HEX' :");
        editText2.setHint("Escribe y toca 'HEX-BIN' :");
        editText1.setKeyListener(DigitsKeyListener.getInstance("01"));
        editText2.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        image1.setImageResource(R.drawable.bintohex);
        image2.setImageResource(R.drawable.hextobin);

        final InputFilter inputFilter_moodMsg = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.length()>44) return "";// max 44chars

                for (int i = start; i < end; i++) {
                    if (source.charAt(i)!='1'
                            && source.charAt(i)!='2'
                            && source.charAt(i)!='3'
                            && source.charAt(i)!='4'
                            && source.charAt(i)!='5'
                            && source.charAt(i)!='6'
                            && source.charAt(i)!='7'
                            && source.charAt(i)!='8'
                            && source.charAt(i)!='9'
                            && source.charAt(i)!='A'
                            && source.charAt(i)!='B'
                            && source.charAt(i)!='C'
                            && source.charAt(i)!='D'
                            && source.charAt(i)!='E'
                            && source.charAt(i)!='F'
                            && source.charAt(i)!='a'
                            && source.charAt(i)!='b'
                            && source.charAt(i)!='c'
                            && source.charAt(i)!='d'
                            && source.charAt(i)!='e'
                            && source.charAt(i)!='f')
                    {
                        return "";
                    }
                }
                return null;
            }
        };
        editText2.setFilters(new InputFilter[] { inputFilter_moodMsg });
        state=3;
    }

}